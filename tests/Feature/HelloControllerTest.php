<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HelloControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample_200()
    {
        $response = $this->get('api/greeting/testing');
        $response->assertStatus(200);
        $response->assertJson(['data' => 'testing']);
    }

    public function testExample_404()
    {
        $test = null;
        $response = $this->get('api/greeting/null');
        $response->assertStatus(404);
        $response->assertJson(['data' => '']);
    }
}
