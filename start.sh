#!/usr/bin/env bash

set -e

if [ "$APP_ENV" == "local" ]; then
    exec apache2-foreground
fi

if [ "$ROLE" == "app" ]; then

    exec apache2-foreground

elif [ "$ROLE" == "queue" ]; then

    echo "Queue role"
    exit 1

elif [ "$ROLE" == "scheduler" ]; then

    while [ true ]
    do
      php /var/www/html/artisan schedule:run --verbose --no-interaction &
      sleep 60
    done

else
    echo "Could not match the container role \"$role\""
    exit 1
fi
